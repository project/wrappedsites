================================================================================
                                Wrapped Sites Theme 
================================================================================
A Wrapped Sites theme for grid-960 based sub-themes. Developed by wrappedsites.com.
================================================================================


Wrapped Sites Theme Information
================================================================================
The Wrapped Sites Theme is a powerful and free Drupal theme based on the 960gs. 
It harnesses the power and features of many popular themes to provide an 
excellent base theme, and sub-theming system to help you quickly prototype 
and theme your site...


Creating your Wrapped Sites Sub Theme
================================================================================

1.  Get the Wrapped Sites theme from drupal.org repository

2.  Duplicate WrpStarter directory from wrappedsites/themes and rename it to the name of your choice.
    (subtheme for this example)
    
3.  Rename WrpStarter.info to yourthemename.info and modify default
    information in the .info file as needed 
    
4.  Visit admin/appearance/settings/yourthemename and configure your subtheme!!

